#pragma once

#define  ERROR_MSG _T("失败了:-( \r\n可能是:\r\n1.这个不是IE窗口\r\n2.不知什么原因获得document失败\r\n3.如果是sogou浏览器注意当前是不是Webkit核")
#include "mainInterfaces.h"
namespace utility
{
	bool ExecuteJavascript(IMainService * pService,BSTR bstrCode);
	bool GetResourceCode(UINT nID, std::vector<char> * pOutVector, LPCTSTR szType = L"JS");
	bool IsIEWindow(CWindow &wnd);
	bool GetHTMLElementRect(IHTMLElement * pElement, CRect * pOutRect);
	bool Copy(LPCTSTR szData, HWND hWnd);
	HWND SmallestWindowFromPoint( const POINT point );


}