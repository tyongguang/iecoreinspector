#pragma once

struct TipsData
{
	UINT nControlID;
	wchar_t * lpTips;
};

class IMainService
{
public:
	//���doc����
	virtual  bool GetDocument(IHTMLDocument2 ** pOutDoc2) = 0;
	virtual  CWindow GetCurHWND() = 0;
	virtual  CWindow GetMainWnd() = 0;
	virtual  void  HighlightCurWndRect(CRect * prcDraw) = 0;
	virtual  void RegisterTips(CWindow wnd, TipsData * pTipsData, int nCount) = 0;
	virtual  void SetNewWnd(CWindow hWnd) = 0;
	virtual  void SetNewHTMLelement(IHTMLElement * pElement) = 0;
};

class IClientNotify
{
public:
	virtual ~IClientNotify()
	{
	};
	virtual void HTMLElementChanged(IHTMLElement * pOutHTMLElement){}
	virtual void DragBegin(){}
	virtual void DragEnd(){}
	virtual void HWNDChanged(CWindow hWnd){}

};


