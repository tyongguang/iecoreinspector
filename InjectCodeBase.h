#pragma once
#include <Windows.h>

typedef LRESULT		(WINAPI *SendMessage_Type)(HWND,UINT,WPARAM,LPARAM);
typedef LRESULT		(WINAPI *PostMessage_Type)(HWND,UINT,WPARAM,LPARAM);
typedef LRESULT		(WINAPI *GetDlgItem_Type)(HWND,int);

typedef LRESULT		(WINAPI *LoadLibrary_Type)(LPCTSTR);
typedef LRESULT		(WINAPI *GetProcAddress_Type)(HMODULE, LPCSTR);


class DataBase
{
public:
	virtual ~DataBase(){}
	SendMessage_Type fnSendMessage;
	PostMessage_Type fnPostMessage;
	GetDlgItem_Type fnGetDlgItem;
	
	LoadLibrary_Type fnLoadLibrary;
	GetProcAddress_Type fnGetProcAddress;

	HWND hWnd;
	BYTE	byResult[128 * sizeof(TCHAR)];

};
class CInjectCodeBase
{
public:
	CInjectCodeBase(void);
	~CInjectCodeBase(void);
	void Inject(HWND hWnd, HANDLE hProcess = INVALID_HANDLE_VALUE);

	DataBase * m_pParamData;
protected:
	virtual void InitData(HWND hWnd);
	void InitDataBase(DataBase * pData);
	virtual LPTHREAD_START_ROUTINE GetThreadProc() = 0;
	virtual int GetThreadProcLen() = 0;
	virtual DataBase * GetParamData();
	virtual int GetParamDataLen();

	BOOL m_bWaitResult;


};
