
#include "InjectCodeBase.h"

CInjectCodeBase::CInjectCodeBase(void)
:m_pParamData(NULL)
,m_bWaitResult(TRUE)
{
}

CInjectCodeBase::~CInjectCodeBase(void)
{
	delete m_pParamData;
}

void CInjectCodeBase::Inject( HWND hWnd, HANDLE hProcess)		
{
	if (INVALID_HANDLE_VALUE == hProcess)
	{
		DWORD PID, TID;
		TID = ::GetWindowThreadProcessId (hWnd, &PID);

		if (PID == GetCurrentProcessId())
		{
		//	ATLASSERT(!"搞自己的进程可能会死锁");
			return ;
		}
		hProcess = 
			OpenProcess(
			PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
			FALSE, PID);
	
	}

	void		*pDataRemote;	// the address (in the remote process) where INJDATA will be copied to;
	DWORD		*pCodeRemote;	// the address (in the remote process) where ThreadFunc will be copied to;
	HANDLE		hThread = NULL; // the handle to the thread executing the remote copy of ThreadFunc;
	DWORD		dwThreadId = 0;

	int		  nCharsXferred = 0; // number of chars retrieved by WM_GETTEXT in the remote thread;
	DWORD dwNumBytesXferred = 0; // number of bytes written/read to/from the remote process;


	__try {
		InitData(hWnd);
	
		LPTHREAD_START_ROUTINE pThreadProc = GetThreadProc();
		int cbCodeSize = GetThreadProcLen();
		int cbParamDataLen = GetParamDataLen();
		void * pParamData = GetParamData();

		// 1. Allocate memory in the remote process for INJDATA
		// 2. Write a copy of DataLocal to the allocated memory
		pDataRemote =  VirtualAllocEx( hProcess, 0, cbParamDataLen, MEM_COMMIT, PAGE_READWRITE );
		if (pDataRemote == NULL)
			__leave;
		WriteProcessMemory( hProcess, pDataRemote, pParamData, cbParamDataLen, &dwNumBytesXferred );


		// Calculate the number of bytes that ThreadFunc occupies
	//	const int cbCodeSize = ((LPBYTE) AfterThreadFunc - (LPBYTE) ThreadFunc);

		// 1. Allocate memory in the remote process for the injected ThreadFunc
		// 2. Write a copy of ThreadFunc to the allocated memory
		pCodeRemote = (PDWORD) VirtualAllocEx( hProcess, 0, cbCodeSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE );		
		if (pCodeRemote == NULL)
			__leave;
		WriteProcessMemory( hProcess, pCodeRemote, pThreadProc, cbCodeSize, &dwNumBytesXferred );


		// Start execution of remote ThreadFunc
		hThread = CreateRemoteThread(hProcess, NULL, 0, 
			(LPTHREAD_START_ROUTINE) pCodeRemote,
			pDataRemote, 0 , &dwThreadId);
		
		if (FALSE == m_bWaitResult)
			return ;

		if (hThread == NULL)
			__leave;

		if (FALSE == m_bWaitResult)
			return ;

		WaitForSingleObject(hThread, INFINITE);

		// Get result (password) back
		ReadProcessMemory( hProcess, pDataRemote, pParamData, cbParamDataLen, &dwNumBytesXferred);
		VirtualFreeEx( hProcess, pDataRemote, 0, MEM_RELEASE );

		VirtualFreeEx( hProcess, pCodeRemote, 0, MEM_RELEASE );

		GetExitCodeThread(hThread, (PDWORD) &nCharsXferred);
		CloseHandle(hThread);			
		
	}
	__finally {
		if ( pDataRemote != 0 )
			VirtualFreeEx( hProcess, pDataRemote, 0, MEM_RELEASE );

		if ( pCodeRemote != 0 )
			VirtualFreeEx( hProcess, pCodeRemote, 0, MEM_RELEASE );

		if ( hThread != NULL ) {
			GetExitCodeThread(hThread, (PDWORD) &nCharsXferred);
			CloseHandle(hThread);			
		}
	}

}

void CInjectCodeBase::InitDataBase(DataBase * pData)
{
	HINSTANCE hUser32 = GetModuleHandle(__TEXT("user32"));
	if (hUser32 != NULL)
	{
		pData->fnSendMessage = (SendMessage_Type)GetProcAddress(hUser32, "SendMessageW");
		pData->fnPostMessage = (PostMessage_Type)GetProcAddress(hUser32, "PendMessageW");
		pData->fnGetDlgItem = (GetDlgItem_Type)GetProcAddress(hUser32, "GetDlgItemW");
	}
	
	HINSTANCE hKernel32 = GetModuleHandle(__TEXT("Kernel32"));
	if (hKernel32 != NULL)
	{
		pData->fnLoadLibrary = (LoadLibrary_Type)GetProcAddress(hKernel32, "LoadLibraryW");
		pData->fnGetProcAddress = (GetProcAddress_Type)GetProcAddress(hKernel32, "GetProcAddressW");
	}


}

void CInjectCodeBase::InitData(HWND hWnd)
{
	m_pParamData = new DataBase();
	m_pParamData->hWnd = hWnd;
	InitDataBase(m_pParamData);
}

DataBase * CInjectCodeBase::GetParamData()
{
	return m_pParamData;
}

int CInjectCodeBase::GetParamDataLen()
{
	return sizeof(DataBase);
}
