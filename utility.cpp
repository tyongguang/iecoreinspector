#include "stdafx.h"
#include "utility.h"

namespace utility{

bool ExecuteJavascript( IMainService * pService,BSTR bstrCode)
{
	HWND hMsgParent = ::GetActiveWindow();
	CComPtr<IHTMLDocument2> spDoc2;
	if (!pService->GetDocument(&spDoc2))
	{
		MessageBox(hMsgParent, ERROR_MSG, _T("发生错误了"), MB_ICONERROR|MB_OK);
		return false;
	}


	CComDispatchDriver spDisp;
	spDoc2->get_Script( &spDisp );
	CComPtr<IHTMLWindow2> spWindow;
	spWindow = spDisp; 
	CComVariant varRet;
	if (!spWindow)
	{
		return false;
	}
	HRESULT hr = spWindow->execScript(bstrCode , CComBSTR(L"JavaScript"), &varRet);


	if (FAILED(hr))
	{
		CString strError;
		strError.Format(_T("Javascript执行发生错误,返回值:%08X"), hr);
		MessageBox(hMsgParent, strError, _T("发生错误了"), MB_ICONERROR|MB_OK);
		return false;

	}

	return true;
}

bool GetResourceCode( UINT nID, std::vector<char> * pOutVector, LPCTSTR szType)
{
	CResource jsCore;
	if (!jsCore.Load(szType, nID))
		return false;

	char * pCharBegin =(char *) jsCore.Lock();
	int nSize = jsCore.GetSize();

	std::vector<char> vecchar;
	pOutVector->reserve(nSize + 10);
	pOutVector->assign(pCharBegin, pCharBegin + nSize);
	pOutVector->push_back(0);

	return true;
}

bool IsIEWindow(CWindow &wnd)
{
	const TCHAR szIEClassName [] = _T("Internet Explorer_Server");
	CString str;
	::GetClassName(wnd, str.GetBuffer(MAX_PATH), MAX_PATH);
	if (str != szIEClassName)
		return false;
	return true;
}

bool GetHTMLElementRect( IHTMLElement * pElement, CRect * pOutRect )
{

	CComQIPtr<IHTMLElement2> spEle2(pElement);
	if (!spEle2)
		return false;

	CComPtr<IHTMLRect> spRect;
	spEle2->getBoundingClientRect(&spRect);
	if (!spRect)
		return false;

	if (pOutRect)
	{
		spRect->get_left(&pOutRect->left);
		spRect->get_right(&pOutRect->right);
		spRect->get_top(&pOutRect->top);
		spRect->get_bottom(&pOutRect->bottom);
	}
	return true;
}


bool Copy(LPCTSTR szData, HWND hWnd)
{
	HANDLE hMem;
	CStringA strData;
	strData = CT2A(szData);

	HGLOBAL globalMem=::GlobalAlloc (GHND,strData.GetLength()+1);
	hMem=::GlobalLock (globalMem);
	memcpy(hMem, (LPCSTR)strData, strData.GetLength());
	::GlobalUnlock (hMem);
	if (!::OpenClipboard (hWnd))
		return false;
	::EmptyClipboard ();
	SetClipboardData (CF_TEXT,hMem);
	::CloseClipboard ()	;
	return true;
}

HWND SmallestWindowFromPoint( const POINT point )
{	
	RECT rect, rcTemp;
	HWND hParent, hWnd, hTemp;

	hWnd = ::WindowFromPoint( point );
	if( hWnd != NULL )
	{
		::GetWindowRect( hWnd, &rect );
		hParent = ::GetParent( hWnd );

		// Has window a parent?
		if( hParent != NULL )
		{
			// Search down the Z-Order
			hTemp = hWnd;
			do{
				hTemp = ::GetWindow( hTemp, GW_HWNDNEXT );

				// Search window contains the point, hase the same parent, and is visible?
				::GetWindowRect( hTemp, &rcTemp );
				if(::PtInRect(&rcTemp, point) && ::GetParent(hTemp) == hParent && ::IsWindowVisible(hTemp))
				{
					// Is it smaller?
					if(((rcTemp.right - rcTemp.left) * (rcTemp.bottom - rcTemp.top)) < ((rect.right - rect.left) * (rect.bottom - rect.top)))
					{
						// Found new smaller window!
						hWnd = hTemp;
						::GetWindowRect(hWnd, &rect);
					}
				}
			}while( hTemp != NULL );
		}
	}

	return hWnd;
}

}

