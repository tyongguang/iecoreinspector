#pragma  once
#include "mainInterfaces.h"
__declspec(selectany) TCHAR * g_attribName[]=
{
	L"innerHTML"
	,L"href"
	,L"src"
	,L"value"
	,L"name"
	,L"style"
	,L"type"
	,L"align"
	,L"width"
	,L"height"
	,L"left"
	,L"top"
	,L"class"
	,L"readonly"
	,L"size"
	,L"id"
	,L"contentEditable"
	,L"---------"
	,L"onclick"
	,L"onchange"
	,L"onkeydown"
	,L"onkeyup"
	,L"onkeypress"
	,L"onmousedown"
	,L"onmouseup"
	,L"onmousemove"
	,L"onmouseout"
	,L"onmouseover"
	,L"onsubmit"
	,L"onreset"
	,L"onselect"
	,L"onload"
};

__declspec(selectany) TipsData g_MainDlgTips[] =
{
	{IDC_EDIT_HWND, L"窗口句柄（十六进制）"}
	,{IDC_EDIT_HWND_DEC, L"窗口句柄（十进制）"}
	,{IDC_EDIT_CLASSNAME, L"窗口类名"}
	,{IDC_STATIC_TARGETICON, L"拖动此图标，捕获目标窗口或IE元素"}
	,{IDC_CHECK_TOPMOST, L"是否置顶"}
	,{IDC_BTN_TOHTMLELEMENT, L"直接遍历到网页的<HTML>节点"}
	,{IDC_BTN_PARENTNODE, L"当前结点的父结点"}
	,{IDC_BTN_BACK, L"返回后一个结点"}
	,{IDC_BTN_BACK2, L"遍历到一开始的结点"}
	,{IDC_EDIT_OFFSET, L"热键(按下ScrollLock)的鼠标偏移,拖放操作的捕获不作偏移处理"}
	,{IDC_STATIC_TARGETICON, L"拖动图标，捕获窗口或者按下ScrolllLock捕获\r\n如果两次按下的窗口都一样的话，将会高亮"}

};


__declspec(selectany) TipsData g_HTMLToolsPageTips[] =
{
	{IDC_BTN_GETIESRC, L"获是IE渲染之后的代码(跟右键源码是有区别的)"}
	,{IDC_BTN_IECOREVERSION, L"通过CSS Hack得前IE当前渲染的内核版本，注意将在网页内显示的文字及文字的颜色，仅限于IE6及以上"}
	,{IDC_BTN_SAVE_CURRENT_SRC, L"把IE当前渲染后的源码保存为样本"}
	,{IDC_BTN_DIFF_PREFILE, L"与之前保存的样本进行比较，前提是必须安装了VS2005的默认安装windiff"}
	,{IDC_BTN_IECMD, L"执行MSHTML的IOleCommandTarget接口的指令"}
	,{IDC_BTN_DOCMODE, L"通过document.documentMode得到IE当前渲染的内核版本"}
};


__declspec(selectany) TipsData g_HTMLCommonPageTips[] =
{
	{IDC_CHECK_HLFOLLOW, L"随前下面操作的变化而高亮当前的HTML结点"}
	,{IDC_BTN_HIGHLIGHT_NODE, L"高亮当前的HTML结点"}
	,{IDC_BTN_REFLASH, L"刷新下面编辑框中当前结点的信息"}
	,{IDC_BTN_HIDE_NODE, L"隐藏显示当前结点"}
	,{IDC_EDIT_RECT, L"当前结点的RECT，分别是left,top,right,bottom"}
	,{IDC_EDIT_ID, L"当前结点的ID"}
	,{IDC_EDIT_IE_CLASSNAME, L"当前结点的CLASS"}
	,{IDC_EDIT_ITEM_CSS, L"当前结点的CSS"}
	,{IDC_BTN_SETCSS, L"修改当前结点的CSS"}
	,{IDC_EDIT_ITEM_CODE, L"当前结点的HTML代码"}
	,{IDC_EDIT_TAGNAME, L"attribute name"}
	,{IDC_EDIT_ATTRIB, L"attribute value"}
	,{IDC_BTN_READ_ATTRIB, L"读取结点的attribute value"}
	,{IDC_BTN_SETATTRIB, L"设置结点的attribute value"}


};

__declspec(selectany) TipsData g_wndPropertyPageTips[] =
{
	{IDC_LIST_STYLE, L"目标窗口的Style"}
	,{IDC_LIST_STYLEEX, L"目标窗口的StyleEx"}
	,{IDC_LIST_CONTROL, L"目标窗口的特有Style"}
	,{IDC_COMBO_CLASSTYPE, L"窗口的类名，一会随着目标的窗口的变化而更新。\r\n可以手动修改此值，点击刷新，而查看其控件风格及代码生成"}
	,{IDC_EDIT_CREATE, L"窗口创建代码的生成，但仅限于左侧Combox内的窗口类"}
	,{IDC_EDIT_THREAD, L"目标窗口所属于的线程ID(十六进制)"}
	,{IDC_EDIT_PROCESS, L"目标窗口所属于的进程ID(十进制)"}


};

__declspec(selectany) TipsData g_wndTestPageTips[] =
{
	{IDC_BTN_SETPARENT, L"将当前目标窗口的父窗口设为上面编辑框的窗口"}
	,{IDC_EDIT_TARGET, L"测试窗口句柄（十进制）"}
	,{IDC_EDIT_SENDMESSAGE_MSG, L"可以是WM_XX系统消息，也可以是消息所对应的数字（十进制）"}
	,{IDC_BTN_REMOTE_THREAD_PWD, L"远程线程注入，以获得密码框密码"}
	,{IDC_BTN_REMOTE_THREAD_CRASH, L"远程线程注入，让目标窗口进程崩溃"}
	
};



struct cssAttribute
{
	wchar_t * szName;
	wchar_t * szTips;
};

__declspec(selectany) cssAttribute g_cssNameTips[] =
{
	{L"accelerator", L"Retrieves a string that indicates whether the object contains an accelerator key."}
	,{L"backgroundAttachment", L"Retrieves how the background image is attached to the object within the document."}
	,{L"backgroundColor", L"Retrieves the color behind the content of the object."}
	,{L"backgroundImage", L"Retrieves the background image of the object."}
	,{L"backgroundPositionX", L"Retrieves the x-coordinate of the IHTMLStyle::backgroundPosition property."}
	,{L"backgroundPositionY", L"Retrieves the y-coordinate of the IHTMLStyle::backgroundPosition property."}
	,{L"backgroundRepeat", L"Retrieves how the backgroundImage property of the object is tiled."}
//	,{L"behavior", L"Retrieves the location of the Dynamic HTML (DHTML) behavior."}
	,{L"blockDirection", L"Gets a string value that indicates whether the content in the block element flows from left to right, or from right to left."}
	,{L"borderBottomColor", L"Retrieves the color of the bottom border of the object."}
	,{L"borderBottomStyle", L"Retrieves the style of the bottom border of the object."}
	,{L"borderBottomWidth", L"Retrieves the width of the bottom border of the object."}
	,{L"borderCollapse", L"Retrieves a value that indicates whether the row and cell borders of a table are joined in a single border or detached as in standard HTML."}
	,{L"borderColor", L"Retrieves the border color of the object."}
	,{L"borderLeftColor", L"Retrieves the color of the left border of the object."}
	,{L"borderLeftStyle", L"Retrieves the style of the left border of the object."}
	,{L"borderLeftWidth", L"Retrieves the width of the left border of the object."}
	,{L"borderRightColor", L"Retrieves the color of the right border of the object."}
	,{L"borderRightStyle", L"Retrieves the style of the right border of the object."}
	,{L"borderRightWidth", L"Retrieves the width of the right border of the object."}
	,{L"borderStyle", L"Retrieves the style of the left, right, top, and bottom borders of the object."}
	,{L"borderTopColor", L"Retrieves the color of the top border of the object."}
	,{L"borderTopStyle", L"Retrieves the style of the top border of the object."}
	,{L"borderTopWidth", L"Retrieves the width of the top border of the object."}
	,{L"borderWidth", L"Retrieves the width of the left, right, top, and bottom borders of the object."}
	,{L"bottom", L"Retrieves the bottom position of the object in relation to the bottom of the next positioned object in the document hierarchy."}
	,{L"clear", L"Retrieves whether the object allows floating objects on its left side, right side, or both, so that the next text displays past the floating objects."}
	,{L"clipBottom", L"Gets the bottom coordinate of the object clipping region."}
	,{L"clipLeft", L"Gets the left coordinate of the object clipping region."}
	,{L"clipRight", L"Gets the right coordinate of the object clipping region."}
	,{L"clipTop", L"Gets the top coordinate of the object clipping region."}
	,{L"color", L"Retrieves the color of the text of the object."}
	,{L"cursor", L"Retrieves the type of cursor to display as the mouse pointer moves over the object."}
	,{L"direction", L"Retrieves the reading order of the object."}
	,{L"display", L"Retrieves whether the object is rendered."}
	,{L"fontFamily", L"Retrieves the name of the font used for text in the object."}
	,{L"fontSize", L"Retrieves a value that indicates the font size used for text in the object."}
	,{L"fontStyle", L"Retrieves the font style of the object as italic, normal, or oblique."}
	,{L"fontVariant", L"Retrieves whether the text of the object is in small capital letters."}
//	,{L"fontWeight", L"Gets the numeric weight of the font of the object."}
	,{L"height", L"Retrieves the height of the object."}
//	,{L"imeMode", L"Retrieves the state of an Input Method Editor (IME)."}
	,{L"layoutGridChar", L"Retrieves the size of the character grid used for rendering the text content of an element."}
	,{L"layoutGridLine", L"Retrieves the gridline value used for rendering the text content of an element."}
	,{L"layoutGridMode", L"Retrieves whether the text layout grid uses two dimensions."}
	,{L"layoutGridType", L"Retrieves the type of grid used for rendering the text content of an element."}
	,{L"left", L"Retrieves the position of the object relative to the left edge of the next positioned object in the document hierarchy."}
	,{L"letterSpacing", L"Retrieves the amount of additional space between letters in the object."}
	,{L"lineBreak", L"Retrieves line-breaking rules for Japanese text."}
	,{L"lineHeight", L"Retrieves the distance between lines in the object."}
	,{L"listStyleImage", L"Retrieves a value that indicates which image to use as a list-item marker for the object."}
	,{L"listStylePosition", L"Retrieves a variable that indicates how the list-item marker is drawn relative to the content of the object."}
	,{L"listStyleType", L"Retrieves the predefined type of the line-item marker for the object."}
	,{L"margin", L"Retrieves the width of the top, right, bottom, and left margins of the object."}
	,{L"marginBottom", L"Retrieves the height of the bottom margin of the object."}
	,{L"marginLeft", L"Retrieves the width of the left margin of the object."}
	,{L"marginRight", L"Retrieves the width of the right margin of the object."}
	,{L"marginTop", L"Retrieves the height of the top margin of the object."}
	,{L"overflow", L"Retrieves a value indicating how to manage the content of the object when the content exceeds the height or width of the object."}
	,{L"overflowX", L"Retrieves how to manage the content of the object when the content exceeds the width of the object."}
	,{L"overflowY", L"Retrieves how to manage the content of the object when the content exceeds the height of the object."}
	,{L"padding", L"Retrieves the amount of space to insert between the object and its margin or, if there is a border, between the object and its border."}
	,{L"paddingBottom", L"Retrieves the amount of space to insert between the bottom border of the object and the content."}
	,{L"paddingLeft", L"Retrieves the amount of space to insert between the left border of the object and the content."}
	,{L"paddingRight", L"Retrieves the amount of space to insert between the right border of the object and the content."}
	,{L"paddingTop", L"Retrieves the amount of space to insert between the top border of the object and the content."}
	,{L"pageBreakAfter", L"Retrieves a value indicating whether a page break occurs after the object."}
	,{L"pageBreakBefore", L"Retrieves a string indicating whether a page break occurs before the object."}
	,{L"position", L"Retrieves the type of positioning used for the object."}
	,{L"right", L"Retrieves the position of the object relative to the right edge of the next positioned object in the document hierarchy."}
	,{L"rubyAlign", L"Retrieves the position of the ruby text specified by the rt object."}
	,{L"rubyOverhang", L"Retrieves the position of the ruby text specified by the rt object."}
	,{L"rubyPosition", L"Retrieves the position of the ruby text specified by the rt object."}
	,{L"styleFloat", L"Retrieves on which side of the object the text will flow."}
	,{L"tableLayout", L"Retrieves a string that indicates whether the table layout is fixed."}
	,{L"textAlign", L"Retrieves whether the text in the object is left-aligned, right-aligned, centered, or justified."}
	,{L"textAutospace", L"Retrieves the autospacing and narrow space width adjustment of text."}
	,{L"textDecoration", L"Retrieves a value that indicates whether the text in the object has blink, line-through, overline, or underline decorations."}
	,{L"textIndent", L"Retrieves the indentation of the first line of text in the object."}
	,{L"textJustify", L"Retrieves the type of alignment used to justify text in the object."}
//	,{L"textJustifyTrim", L"Not currently implemented."}
	,{L"textKashida", L"Not currently implemented."}
	,{L"textTransform", L"Retrieves the rendering of the text in the object."}
	,{L"top", L"Retrieves the position of the object relative to the top of the next positioned object in the document hierarchy."}
	,{L"unicodeBidi", L"Retrieves the level of embedding with respect to the bidirectional algorithm."}
	,{L"verticalAlign", L"Retrieves the vertical alignment of the object."}
	,{L"visibility", L"Sets or retrieves whether the content of the object is displayed."}
	,{L"width", L"Retrieves the width of the object."}
	,{L"wordBreak", L"Retrieves line-breaking behavior within words, particularly where multiple languages appear in the object."}
	,{L"zIndex", L"Retrieves the stacking order of positioned objects."}
//	,{L"filter", L"Retrieves the filter or collection of filters applied to the object."}
//	,{L"hasLayout", L"Gets a value that indicates whether the object has layout."}
//	,{L"isBlock", L"Gets a value that indicates whether the object is a block-level object."}
	,{L"layoutFlow", L"Retrieves the direction and flow of the content in the object."}
	,{L"scrollbar3dLightColor", L"Retrieves the color of the top and left edges of the scroll box and scroll arrows of a scroll bar."}
	,{L"scrollbarArrowColor", L"Retrieves the color of the arrow elements of a scroll arrow."}
	,{L"scrollbarBaseColor", L"Retrieves the color of the main elements of a scroll bar, which include the scroll box, track, and scroll arrows."}
	,{L"scrollbarDarkShadowColor", L"Retrieves the color of the gutter of a scroll bar."}
	,{L"scrollbarFaceColor", L"Retrieves the color of the scroll box and scroll arrows of a scroll bar."}
	,{L"scrollbarHighlightColor", L"Retrieves the color of the top and left edges of the scroll box and scroll arrows of a scroll bar."}
	,{L"scrollbarShadowColor", L"Retrieves the color of the bottom and right edges of the scroll box and scroll arrows of a scroll bar."}
//	,{L"scrollbarTrackColor", L"Retrieves the color of the track element of a scroll bar."}
	,{L"textAlignLast", L"Retrieves how to align the last line or only line of text in the object."}
	,{L"textKashidaSpace", L"Retrieves the ratio of kashida expansion to white space expansion when justifying lines of text in the object."}
	,{L"textUnderlinePosition", L"Retrieves the position of the underline decoration that is set through the IHTMLStyle::textDecoration property of the object."}
//	,{L"wordWrap", L"Retrieves whether to break words when the content exceeds the boundaries of its container."}
	,{L"writingMode", L"Retrieves the direction and flow of the content in the object."}
	,{L"zoom", L"Retrieves the magnification scale of the object."}
	,{L"minHeight", L"Retrieves the minimum height for an element."}
	,{L"textOverflow", L"Sets or retrieves a value that indicates whether to render ellipses(...)to indicate text overflow."}
	,{L"whiteSpace", L"Retrieves a value that indicates whether lines are automatically broken inside the object."}
	,{L"wordSpacing", L"Retrieves the amount of additional space between words in the object."}
	,{L"maxHeight", L"Retrieves the maximum height for an element."}
	,{L"maxWidth", L"Retrieves the maximum width for an element."}
	,{L"minWidth", L"Retrieves the minimum width for an element."}
//	,{L"msInterpolationMode", L"Retrieves the interpolation (resampling) method used to stretch images."}

};
