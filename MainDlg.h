// MainDlg.h : interface of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include <atlcrack.h>
#include <atlmisc.h>
#if !defined(VFC_MAINDLG_H__8AEB60F6_C56C_4378_82A7_49468514D15F__INCLUDED_)
#define VFC_MAINDLG_H__8AEB60F6_C56C_4378_82A7_49468514D15F__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "utility.h"


class CMyPropertySheet:public CPropertySheetImpl<CMyPropertySheet>
{
public:
	CMyPropertySheet()
	{
		m_psh.dwFlags |= PSH_NOAPPLYNOW;
	}

	static int CALLBACK PropSheetCallback(HWND hWnd, UINT uMsg, LPARAM lParam)
	{
		// dialog template is available and changeable pre-creation
		if(uMsg == PSCB_PRECREATE)
		{
			LPDLGTEMPLATE lpDT = (LPDLGTEMPLATE)lParam;

			// remove dialog border styles
			lpDT->style -= DS_MODALFRAME | WS_POPUP | WS_CAPTION | WS_SYSMENU;

			// add child window and clipping styles
			lpDT->style |= WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

			lpDT->dwExtendedStyle |= WS_EX_CONTROLPARENT;
			return 0;
		}
		else // call inherited method
			return CPropertySheetImpl<CMyPropertySheet>::PropSheetCallback(hWnd, uMsg, lParam);
	}
};
class CMainDlg
	:public CDialogImpl<CMainDlg>
	,public CMessageFilter
	,public IMainService
{
public:
	enum { IDD = IDD_MAINDLG };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	//implement IMainService
	virtual bool GetDocument(IHTMLDocument2 ** ppOutDoc2);	//��IE���ڽ�����false
	virtual CWindow GetCurHWND();
	virtual CWindow GetMainWnd();
	virtual void  HighlightCurWndRect(CRect * prcDraw);
	virtual void RegisterTips(CWindow wnd, TipsData * pTipsData, int nCount);
	virtual  void SetNewWnd(CWindow hWnd);
	virtual  void SetNewHTMLelement(IHTMLElement * pElement);
	CMainDlg();
	~CMainDlg();

	BEGIN_MSG_MAP(CMainDlg)
		COMMAND_HANDLER_EX(IDC_BTN_HEIGHTLIGHT, BN_CLICKED, OnBnClickedBtnHeightlight)
		COMMAND_HANDLER_EX(IDC_BTN_PARENT, BN_CLICKED, OnBtnClickGetParent)
		COMMAND_HANDLER_EX(IDC_CHECK_TOPMOST, BN_CLICKED, OnCheckTopmostBnClicked)
		COMMAND_HANDLER_EX(IDC_BTN_PARENTNODE, BN_CLICKED, OnBtnParentnode)
		COMMAND_HANDLER_EX(IDC_BTN_TOHTMLELEMENT, BN_CLICKED, OnBtnTohtmlelement)
		COMMAND_HANDLER_EX(IDC_BTN_BACK2, BN_CLICKED, OnBtnBack2End)
		COMMAND_HANDLER_EX(IDC_BTN_BACK, BN_CLICKED, OnBtnBack)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MSG_WM_MOUSEMOVE(OnMouseMove)
		MSG_WM_LBUTTONDOWN(OnLButtonDown)
		MSG_WM_LBUTTONUP(OnLButtonUp)
		MSG_WM_SETCURSOR(OnSetCursor)
		MSG_WM_CLOSE(OnClose)
		MESSAGE_HANDLER(WM_HOTKEY, OnHotKey)
	END_MSG_MAP()

	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnMouseMove(UINT Flags, CPoint Pt);
	LRESULT OnLButtonDown(UINT Flags, CPoint Pt);
	LRESULT OnLButtonUp(UINT Flags, CPoint Pt);
	LRESULT OnSetCursor(HWND hwndCursor, UINT codeHitTest, UINT msg);
	LRESULT OnClose(void);
	LRESULT OnBnClickedBtnHeightlight(WORD wNotifyCode, WORD wID, HWND hWndCtl);
	LRESULT OnBtnClickGetParent(WORD wNotifyCode, WORD wID, HWND hWndCtl);
	LRESULT OnCheckTopmostBnClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl);
private:
	void UpdateWindowInfo(CWindow & hWin);
	void SetTopMoust();
	void DrawWindowRect(CWindow& window, CRect * pRC = NULL);
	
	void CreateTabPages();

	LRESULT OnBtnParentnode(WORD wNotifyCode, WORD wID, HWND hWndCtl);
	LRESULT OnBtnBack2End(WORD wNotifyCode, WORD wID, HWND hWndCtl);
	LRESULT OnBtnBack(WORD wNotifyCode, WORD wID, HWND hWndCtl);
	LRESULT OnBtnTohtmlelement(WORD wNotifyCode, WORD wID, HWND hWndCtl);		


	typedef CAdapt<CComPtr<IHTMLElement> > STL_HTMLElement;
	std::stack<STL_HTMLElement, std::list<STL_HTMLElement> > m_stackElement;

	CWindow m_hCurWnd;
	CWindow m_hPreWnd;
	CStatic m_wndStatic;
	WTL::CToolTipCtrl m_tips;
	CMyPropertySheet m_sheet;

	CComPtr<IHTMLElement> m_spPreElement;
	BOOL m_bCapture;
	bool m_bTopMost;

	CRect m_rcLast;
	CRect m_rcLastIE;

	HCURSOR m_hCursor;
	std::list<IClientNotify *> m_listClientNotify;
	typedef std::list<IClientNotify *>::iterator clientNotifyIter;
	bool m_bInFrame;
	bool m_bInIFrame;
	int m_nCurIndex;

public:
	LRESULT OnHotKey(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// VisualFC AppWizard will insert additional declarations immediately before the previous line.

#endif // !defined(VFC_MAINDLG_H__8AEB60F6_C56C_4378_82A7_49468514D15F__INCLUDED_)
