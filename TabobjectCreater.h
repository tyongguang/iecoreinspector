#pragma  once 
#include "mainInterfaces.h"
#include "CommonPage.h"
#include "JSExecuterPage.h"
#include "ToolsPage.h"
#include "BugsPage.h"
#include "CSSPage.h"
#include "WndTestPage.h"
#include "WndDCTest.h"
#include "WndRelative.h"
#include "WndProperty.h"
#include "WndText.h"
#include "WndState.h"


typedef IClientNotify * ( * TypeTabObjectCreater)(IMainService * pMainService, PROPSHEETPAGE ** pPage);

template<class T>
 IClientNotify * TabCreater(IMainService * pMainService, PROPSHEETPAGE ** pPage)
 {
	 T * pTObj = new T(pMainService);
	 *pPage = &pTObj->m_psp;
	 return pTObj;
 }

//所有tab obj对象都在这里添加
__declspec(selectany) TypeTabObjectCreater g_tabObjectCreaters[]=
{
	 TabCreater<CCommonPage>
	,TabCreater<CJSExecuterPage>
	,TabCreater<CToolsPage>
	,TabCreater<CCSSPage>
	,TabCreater<CWndTestPage>
	,TabCreater<CWndDCTestPage>
	,TabCreater<CWndRelative>
	,TabCreater<CWndProperty>
	,TabCreater<CWndState>
	,TabCreater<CWndText>
	,TabCreater<CBugsPage>


};