(function(){
    function newStyle(str){
	    var pa= document.getElementsByTagName('head')[0] ;
	    var el= document.createElement('style');
	    el.type= 'text/css';
	    el.media= 'screen';
	    el.id = '__tyg_css_hack';
	    if(el.styleSheet) el.styleSheet.cssText= str;// IE method
	    else el.appendChild(document.createTextNode(str));// others
	    pa.appendChild(el);
	    return el;
    }

    function CreateVersionDiv(thisID,thisText)
    {
	    var info = document.getElementById(thisID);
	    if (info != null)
	    {
		    info.parentNode.removeChild(info);
	    }
	    info = document.createElement('div');
	    info.id = thisID;

	    info.innerHTML = thisText;
	    document.body.appendChild(info);
	    var rc;
	    rc = info.getBoundingClientRect();
    	
	    info.style.cssText = "fontFamily:sans-serif;background-color:white;font-size:300%;color:red;+color:green;_color:red;position:fixed;left:10px;top:10px;z-index:90000;_position:relative;_top:-"
	                                +(rc.top-10).toString()+"px;_left:-" + (rc.left-10).toString() + "px";
    }
    function removeElement(thisID)
    {
	    var info = document.getElementById(thisID);
	    if (info != null)
	    {
		    info.parentNode.removeChild(info);
	    }
    }

    function removeAllElement()
    {
	    removeElement("tyg_core_ie6");
	    removeElement("tyg_core_ie7");
	    removeElement("tyg_core_ie8");
	    removeElement("tyg_core_ie9");
        removeElement("__tyg_css_hack");
	    
    }
    function main()
    {
        CreateVersionDiv("tyg_core_ie6", "The Render Core is IE6(red) or IE7(green)");
        CreateVersionDiv("tyg_core_ie7", "The Render Core is IE7(green) or IE8(red)");
        CreateVersionDiv("tyg_core_ie8", "The Render Core is IE8");
        CreateVersionDiv("tyg_core_ie9", "The Render Core is IE9");

        var myCSSHack ="#tyg_core_ie6, #tyg_core_ie7, #tyg_core_ie8, #tyg_core_ie9 {display: none}\r\n* html #tyg_core_ie6 {display: block}\r\n*+html #tyg_core_ie7 {display: block}\r\n@media \\0screen {#tyg_core_ie8 {display: block}}\r\n:root #tyg_core_ie9 {display: block\\9}";

        removeElement("__tyg_css_hack");
        newStyle(myCSSHack);
        setTimeout("__tyg_test_removeAllElement()", 5000);
    }
    window.__tyg_test_removeAllElement = removeAllElement;
    window.__tyg_test_main =  main;
})();
__tyg_test_main();

