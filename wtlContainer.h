#pragma once
/*
2010年8月4日
用途：在使用CContainedWindow的时候，需要将BEGIN_MSG_MAP改成BEGIN_MSG_MAP_EX，但这样会导致IDE不能识别这个类，为了让BEGIN_MSG_MAP继续工作，需要将原来
ALT_MSG_MAP换成      BEGIN_CONTAINER_MSG_MAP
然后在这个范围内添加  END_CONTAINER_MSG_MAP
*/
namespace WTL
{
	class MsgHolder
	{
	public:
		MsgHolder(HWND hWnd, UINT uMsg, WPARAM wParamIn, LPARAM lParamIn,
			const _ATL_MSG * * pCurMsg)
			:m_ppCurrentMsg(pCurMsg),m_pOldMsg(* pCurMsg)
		{
			static _ATL_MSG curMsg;
			curMsg.hwnd = hWnd;
			curMsg.message = uMsg;
			curMsg.wParam = wParamIn;
			curMsg.lParam = lParamIn;
			curMsg.time = 0;
			curMsg.pt.x = curMsg.pt.y = 0;
			*m_ppCurrentMsg=&curMsg;
		}
		~MsgHolder()
		{
			*m_ppCurrentMsg=m_pOldMsg;//这么辛苦就是为了这个析构函数
		}
	private:
		const _ATL_MSG ** m_ppCurrentMsg;
		const _ATL_MSG * m_pOldMsg;
	};

#define BEGIN_CONTAINER_MSG_MAP(msgMapID) \
	break; \
		case msgMapID:\
	{\
	MsgHolder msgHoder(hWnd, uMsg, wParam, lParam,&m_pCurrentMsg);

#define END_CONTAINER_MSG_MAP() }
};