// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "mainInterfaces.h"
#include "storeData.h"

#include "MainDlg.h"

#include "TabobjectCreater.h"
#define  HOTKEYID_CAPTURE  1
#define  HOTKEYID_HIGHLIGHT  2


BOOL CMainDlg::PreTranslateMessage( MSG* pMsg )
{
	if (m_tips.IsWindow())
	{
		m_tips.RelayEvent(pMsg);
	}
	return CWindow::IsDialogMessage(pMsg);

}


CMainDlg::CMainDlg() : m_nCurIndex(0)
{
}

CMainDlg::~CMainDlg()
{
	std::list<IClientNotify *>::iterator iter;
	for(iter = m_listClientNotify.begin(); iter != m_listClientNotify.end(); ++ iter)
	{
		delete *iter;
	}
}


bool CMainDlg::GetDocument( IHTMLDocument2 ** ppOutDoc2 )
{
	static UINT nMsg = ::RegisterWindowMessage( _T("WM_HTML_GETOBJECT") );
	if (m_hCurWnd.IsWindow() == FALSE)
		return false;

	CWindow m_wndTarget;
	m_wndTarget.m_hWnd = NULL;
	if (!utility::IsIEWindow(m_hCurWnd))
	{
		m_wndTarget = m_hCurWnd.GetParent();
		if (!utility::IsIEWindow(m_wndTarget))//要检查一下父窗口
		{
			return false;
		}
	}
	if (m_wndTarget.m_hWnd == NULL)
		m_wndTarget = m_hCurWnd;

	LRESULT lRes;
	::SendMessageTimeout( m_wndTarget, nMsg, 0L, 0L, SMTO_ABORTIFHUNG, 1000, (DWORD*) &lRes );

	HRESULT hr = ::ObjectFromLresult ( lRes, IID_IHTMLDocument2, 0 , (LPVOID *) ppOutDoc2 );
	if ( FAILED ( hr ) )
	{
		return false;
	}
	return true;
}

void CMainDlg::HighlightCurWndRect( CRect * prcDraw )
{
	DrawWindowRect(m_hCurWnd, prcDraw);
}

CWindow CMainDlg::GetCurHWND()
{
	return m_hCurWnd;
}

CWindow CMainDlg::GetMainWnd()
{
	return m_hWnd;
}


bool JudgeFrameState(BOOL bInFrame, BOOL bIniFrame, bool bInnerFrame, bool bInnerIFrame)
{
	//元素都不是frame的话,直接不进行
	if (bInnerIFrame ==  false && bInnerFrame == false)
		return false;

	if (bIniFrame && bInnerIFrame)
	{
		return true;
	}

	if (bInFrame && bInnerFrame)
	{
		return true;
	}
	return false;
}

HRESULT GetElemFromPoint(IHTMLDocument2 * pDoc2, POINT ptPos, IHTMLElement ** pElement/*out*/ ,BOOL bInFrame, BOOL bIniFrame)
{

	HRESULT hr;

	IHTMLElement * &refElem = * pElement;

	CComPtr<IHTMLDocument2> spDoc2;
	spDoc2 = pDoc2;

	hr = spDoc2->elementFromPoint(ptPos.x, ptPos.y, &refElem);
	if (FAILED(hr))
		return hr;

	while(refElem)
	{
		CComBSTR bstrTagName;
		hr = refElem->get_tagName(&bstrTagName);
		if (FAILED(hr))
			return hr;

		bool bInnerFrame = 0==_wcsicmp(bstrTagName, L"frame");
		bool bInnerIFrame =  0==_wcsicmp(bstrTagName, L"iframe");

		if (!JudgeFrameState(bInFrame, bIniFrame , bInnerFrame, bInnerIFrame))
			return hr;

		CComQIPtr<IWebBrowser2> spFrameWB2 = refElem;
		if (!spFrameWB2)
			return hr;

		CComDispatchDriver spDispatch;
		hr = spFrameWB2->get_Document(&spDispatch);
		spDoc2 = spDispatch;
		if (!spDoc2)
			return hr;



		CComQIPtr<IDisplayServices> spDispServices;
		spDispServices = spDoc2;
		if (!spDispServices)
			return hr;

		POINT ptLocal = ptPos;
		spDispServices->TransformPoint(&ptLocal, COORD_SYSTEM_GLOBAL, COORD_SYSTEM_FRAME, refElem);

		refElem =  NULL;
		hr = spDoc2->elementFromPoint(ptLocal.x, ptLocal.y, &refElem);
	}
	return hr;
}


void CMainDlg::CreateTabPages()
{
	for(int i = 0; i < _countof(g_tabObjectCreaters); ++ i)
	{
		PROPSHEETPAGE * pPage;
		IClientNotify * obj = g_tabObjectCreaters[i](this, &pPage);

		m_sheet.AddPage((LPCPROPSHEETPAGE)(pPage));
		m_listClientNotify.push_back(obj);
	}

	int nIndex = 0;
	nIndex = GetPrivateProfileInt(L"setting" , L"tabindex" , 0 ,L".\\Inspector_config.ini" );
	m_sheet.SetActivePage(nIndex);

	m_nCurIndex = nIndex;

// 	CCommonPage * pPage=new CCommonPage(this);
// 	m_sheet.AddPage(*pPage);
// 	m_listClientNotify.push_back(pPage);


	m_sheet.Create(m_hWnd);

	CWindow wndHolder = GetDlgItem(IDC_STATIC_HOLDER);
	CRect rc;
	wndHolder.GetClientRect(&rc);
	wndHolder.ClientToScreen(&rc);
	ScreenToClient(&rc);
	m_sheet.SetWindowPos(NULL, rc, SWP_SHOWWINDOW);

	//将两个没用的按钮disable掉
	m_sheet.GetDlgItem(IDOK).ShowWindow(SW_HIDE);
	m_sheet.GetDlgItem(IDCANCEL).ShowWindow(SW_HIDE);

	
	m_sheet.Invalidate();
	CTabCtrl(m_sheet.GetTabControl()).ModifyStyle(TCS_MULTILINE, 0);
}

LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// center the dialog on the screen
	CenterWindow();

	this->ExecuteDlgInit(IDD);
	// set icons
	HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON), LR_DEFAULTCOLOR);
	SetIcon(hIcon, TRUE);
	HICON hIconSmall = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	SetIcon(hIconSmall, FALSE);

	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	m_tips.Create(m_hWnd, CRect(0, 0, 200, 200));
	m_tips.SetMaxTipWidth(200);


	m_hCursor = LoadCursor(_Module.m_hInstResource, MAKEINTRESOURCE(IDC_CURSOR1));


	m_wndStatic = (CStatic)GetDlgItem(IDC_STATIC_TARGETICON); 
	m_wndStatic.SetIcon(m_hCursor);

	m_rcLast.SetRectEmpty();
	m_bCapture = FALSE;

	m_bTopMost = true;

#ifdef _DEBUG
	m_bTopMost = false;
#endif
	if (m_bTopMost)
		((CButton)GetDlgItem(IDC_CHECK_TOPMOST)).SetCheck(1);
	else
		((CButton)GetDlgItem(IDC_CHECK_TOPMOST)).SetCheck(0);


	GetDlgItem(IDC_EDIT_OFFSET).SetWindowText(L"0,0");

	SetTopMoust();

	RegisterHotKey(m_hWnd, HOTKEYID_CAPTURE, 0 , VK_SCROLL);
//	RegisterHotKey(m_hWnd, HOTKEYID_HIGHLIGHT,  MOD_SHIFT, VK_SCROLL);
	
	CreateTabPages();

	RegisterTips(*this, g_MainDlgTips, _countof(g_MainDlgTips));
	//m_sheet.SetParent(m_hWnd);
	return TRUE;
}

LRESULT CMainDlg::OnClose(void)
{
	int nIndex;
	nIndex = m_sheet.GetActiveIndex();
	CString strIndex;
	strIndex.Format(L"%d", nIndex);
	WritePrivateProfileString(L"setting" , L"tabindex" , strIndex ,L".\\Inspector_config.ini" );
	m_sheet.SetActivePage(nIndex);



	CString strScriptPath ;
	GetPrivateProfileString(L"script",L"path",NULL,strScriptPath.GetBuffer(MAX_PATH),MAX_PATH,L".\\TaskPlanconfig.ini");
	strScriptPath .ReleaseBuffer ();


	DestroyWindow();
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);



	UnregisterHotKey(m_hWnd, HOTKEYID_CAPTURE);
//	UnregisterHotKey(m_hWnd, HOTKEYID_HIGHLIGHT);
	PostQuitMessage(0);
	return 0;
}




void CMainDlg::DrawWindowRect( CWindow& window, CRect * pRC)
{
	if (!window.IsWindow())
		return ;
	if (window == m_hWnd)
		return ;

	CPen pen;
	pen.CreatePen(PS_SOLID, 4, RGB(255,11,122) );
	CClientDC dc(NULL);
	int oldRop2Mode = dc.SetROP2(R2_NOTXORPEN );
	HPEN oldpen  = dc.SelectPen(pen);

	if (!pRC)
	{
		CRect rc;
		window.GetWindowRect(&rc);
		dc.Rectangle(rc);
	}
	else
	{
		CRect rcTemp(*pRC);
		window.ClientToScreen(&rcTemp);
		dc.Rectangle(rcTemp);

	}
	dc.SelectPen(oldpen);
	dc.SetROP2(oldRop2Mode);

}


LRESULT CMainDlg::OnMouseMove(UINT Flags, CPoint Pt)
{

	if (!m_bCapture)
		return 0;
	ClientToScreen(&Pt);
	m_hCurWnd = ::utility::SmallestWindowFromPoint(Pt);
	CRect rcWnd;
	m_hCurWnd.GetWindowRect(&rcWnd);
	CComPtr<IHTMLDocument2> spDoc2;
	if (GetDocument(&spDoc2))
	{
		//如果是IE的话
		CComPtr<IHTMLElement> spEle;

		m_hCurWnd.ScreenToClient(&Pt);

		GetElemFromPoint(spDoc2, Pt,  &spEle, m_bInFrame, m_bInIFrame);
		//	spDoc2->elementFromPoint(Pt.x, Pt.y, &spEle);

		CRect rcElement;		
		if (!utility::GetHTMLElementRect(spEle, &rcElement))
			return 0;

		UpdateWindowInfo(m_hCurWnd);
		if (!spEle.IsEqualObject(m_spPreElement))
		{
			for(clientNotifyIter iter = m_listClientNotify.begin();
				iter != m_listClientNotify.end(); ++ iter)
			{
				(*iter)->HTMLElementChanged(spEle);
			}
		}


		if (m_rcLastIE != rcElement)
		{

			if (!utility::IsIEWindow(m_hPreWnd))//如果上一个窗口不是IE
			{
				DrawWindowRect(m_hPreWnd);
			}else
				DrawWindowRect(m_hCurWnd, &m_rcLastIE);

			DrawWindowRect(m_hCurWnd, &rcElement);
		}
		m_spPreElement = spEle;
		m_rcLastIE = rcElement;
		m_rcLast = rcWnd;
		m_hPreWnd = m_hCurWnd;

		for(clientNotifyIter iter = m_listClientNotify.begin();
			iter != m_listClientNotify.end(); ++ iter)
		{
			(*iter)->HWNDChanged(m_hCurWnd);
		}

		return 0;
	}

	if (m_rcLast != rcWnd)
	{
		if (utility::IsIEWindow(m_hPreWnd))
		{
			DrawWindowRect(m_hPreWnd, &m_rcLastIE);
			m_rcLastIE.SetRectEmpty();
		}else
			DrawWindowRect(m_hPreWnd);

	

		for(clientNotifyIter iter = m_listClientNotify.begin();
			iter != m_listClientNotify.end(); ++ iter)
		{
			(*iter)->HWNDChanged(m_hCurWnd);
		}

		DrawWindowRect(m_hCurWnd);
		UpdateWindowInfo(m_hCurWnd);
		m_hPreWnd = m_hCurWnd;

		m_rcLast = rcWnd;

	}
	return 0;
}

LRESULT CMainDlg::OnLButtonDown(UINT Flags, CPoint Pt)
{
	CRect rcWindow;
	CPoint ptOrigin = Pt;


	m_bInFrame =1 ==((CButton)GetDlgItem(IDC_CHECK_THROUGH_FRAME)).GetCheck();
	m_bInIFrame = 1 ==((CButton)GetDlgItem(IDC_CHECK_THROUGH_IFRAME)).GetCheck();

	m_wndStatic.GetWindowRect(&rcWindow);
	ClientToScreen(&ptOrigin);
	if (rcWindow.PtInRect(ptOrigin))
	{
		m_wndStatic.SetIcon(NULL);
		SetCapture();
		m_bCapture = true;
		SetCursor(m_hCursor);
		
		while(!m_stackElement.empty())   
			m_stackElement.pop();

		for(clientNotifyIter iter = m_listClientNotify.begin();
			iter != m_listClientNotify.end(); ++ iter)
		{
			(*iter)->DragBegin();
		}
		return 0;
	}

	return 0;
}

LRESULT CMainDlg::OnLButtonUp(UINT Flags, CPoint Pt)
{
	if (m_hCurWnd.IsWindow() == FALSE)
		return 0;
	m_wndStatic.SetIcon(m_hCursor);
	m_bCapture = false;
	ReleaseCapture();

	if (!utility::IsIEWindow(m_hCurWnd))
		DrawWindowRect(m_hCurWnd);
	else
		DrawWindowRect(m_hCurWnd,&m_rcLastIE);

	DWORD lRes = 0;
	::SendMessageTimeout(m_hCurWnd, WM_PAINT, 0L, 0L, SMTO_ABORTIFHUNG, 1000, (DWORD*) &lRes );

	m_rcLastIE.SetRectEmpty();
	m_rcLast.SetRectEmpty();
	m_hPreWnd = NULL;

	if (m_spPreElement)
		m_stackElement.push(m_spPreElement);

	for(clientNotifyIter iter = m_listClientNotify.begin();
		iter != m_listClientNotify.end(); ++ iter)
	{
		(*iter)->DragEnd();
	}
	return 0;
}

LRESULT CMainDlg::OnSetCursor(HWND hwndCursor, UINT codeHitTest, UINT msg)
{
	if (m_bCapture)
	{
		SetCursor(m_hCursor);
		return TRUE;
	}
	SetMsgHandled(false);
	return false;

}
LRESULT CMainDlg::OnBtnClickGetParent(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	CWindow hTemp = m_hCurWnd.GetParent();
	if (hTemp.IsWindow() == FALSE)
		return 0;

	m_hCurWnd = hTemp;
	UpdateWindowInfo(m_hCurWnd);
	return 0;
}

void CMainDlg::UpdateWindowInfo(CWindow & hWin)
{
	if (hWin.IsWindow() == FALSE)
		return ;
	CString str;
	::GetClassName(hWin, str.GetBuffer(MAX_PATH), MAX_PATH);

	SetDlgItemText(IDC_EDIT_CLASSNAME,str); 

	str.Format(L"0x%08X", hWin.m_hWnd);
	SetDlgItemText(IDC_EDIT_HWND,str); 

	str.Format(L"%08d", hWin.m_hWnd);
	SetDlgItemText(IDC_EDIT_HWND_DEC,str); 

}


LRESULT CMainDlg::OnBnClickedBtnHeightlight( WORD wNotifyCode, WORD wID, HWND hWndCtl )
{
	if (!m_hCurWnd.IsWindow())
		return 0;

	for(int i = 0;i < 3; ++i)
	{
		DrawWindowRect(m_hCurWnd);
		Sleep(100);
		DrawWindowRect(m_hCurWnd);
		Sleep(100);
	}
	return 0;
}

LRESULT CMainDlg::OnCheckTopmostBnClicked(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	m_bTopMost = !m_bTopMost;

	SetTopMoust();
	return 0;
}

void CMainDlg::SetTopMoust()
{
	if (m_bTopMost)
	{
		::SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE|SWP_SHOWWINDOW|SWP_NOMOVE|SWP_NOSIZE);
	}
	else
	{
		::SetWindowPos(m_hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE|SWP_SHOWWINDOW|SWP_NOMOVE|SWP_NOSIZE);
	}
}

void CMainDlg::RegisterTips( CWindow wnd, TipsData * pTipsData, int nCount )
{
	for(int i = 0 ;i < nCount; ++ i)
	{
		m_tips.AddTool(wnd.GetDlgItem(pTipsData[i].nControlID), pTipsData[i].lpTips);
	}
}

void CMainDlg::SetNewWnd( CWindow hWnd )
{
	m_hCurWnd = hWnd;
	for(clientNotifyIter iter = m_listClientNotify.begin();
		iter != m_listClientNotify.end(); ++ iter)
	{
		(*iter)->HWNDChanged(hWnd);
		(*iter)->DragEnd();
	}
	UpdateWindowInfo(hWnd);
}

void CMainDlg::SetNewHTMLelement( IHTMLElement * pElement )
{


	for(clientNotifyIter iter = m_listClientNotify.begin();
		iter != m_listClientNotify.end(); ++ iter)
	{
		(*iter)->HTMLElementChanged(pElement);
	}
}



LRESULT CMainDlg::OnBtnParentnode(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	if (!m_spPreElement)
	{
		MessageBox(_T("结点无效"), _T("发生错误了"), MB_ICONERROR|MB_OK);
		return -1;
	}
	CComPtr<IHTMLElement> spParentElement;
	m_spPreElement->get_parentElement(&spParentElement);
	if (spParentElement ==  NULL)
	{
		//MessageBox(_T("到头了,回头是岸~"), _T("发生错误了"), MB_ICONERROR|MB_OK);

		return -1;
	}

	m_stackElement.push(spParentElement);
	m_spPreElement = spParentElement;
	//UpdateElementInfo(m_spCurElement);
	for(clientNotifyIter iter = m_listClientNotify.begin();
		iter != m_listClientNotify.end(); ++ iter)
	{
		(*iter)->HTMLElementChanged(m_spPreElement);
	}
	return 0;
}

//HTML 元素后退
LRESULT CMainDlg::OnBtnBack(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	if (!m_spPreElement)
	{
		MessageBox(_T("结点无效"), _T("发生错误了"), MB_ICONERROR|MB_OK);
		return -1;
	}
	if (1== m_stackElement.size())
	{
		//	MessageBox(_T("已经到栈底了"), _T("发生错误了"), MB_ICONERROR|MB_OK);
		return -1;
	}
	m_stackElement.pop();
	m_spPreElement = m_stackElement.top();
	
	for(clientNotifyIter iter = m_listClientNotify.begin();
		iter != m_listClientNotify.end(); ++ iter)
	{
		(*iter)->HTMLElementChanged(m_spPreElement);
	}

	return 0;
}

LRESULT CMainDlg::OnBtnTohtmlelement(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	while(-1 != OnBtnParentnode(0,0,0) );
	return 0;
}

LRESULT CMainDlg::OnBtnBack2End(WORD wNotifyCode, WORD wID, HWND hWndCtl)
{
	while(-1 != OnBtnBack(0,0,0) );
	return 0;
}





LRESULT CMainDlg::OnHotKey(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	int HotKeyID = (int)wParam;
	UINT flags = (UINT)LOWORD(lParam);
	UINT vk = (UINT)HIWORD(lParam);

	int nCurIndex = m_sheet.GetActiveIndex();
	CString strPageText;
	CWindow(m_sheet.GetActivePage()).GetWindowText(strPageText);
	if (HOTKEYID_CAPTURE == HotKeyID)
	{

		CString strOffset;
		GetDlgItem(IDC_EDIT_OFFSET).GetWindowText(strOffset);

		int nPos = strOffset.Find(L",");
		CString strX = strOffset.Left(nPos);
		CString strY = strOffset.Mid(nPos+1);
		strX.Trim();
		strY.Trim();

		int x = _wtoi(strX);
		int y = _wtoi(strY);

		CPoint pt;
		GetCursorPos(&pt);
		pt.Offset(x, y);
		m_hCurWnd = utility::SmallestWindowFromPoint(pt);

		if (m_hPreWnd == m_hCurWnd && m_nCurIndex == nCurIndex && strPageText != L"状态")
		{
			OnBnClickedBtnHeightlight(0, 0, 0);
			return 0;
		}

		m_nCurIndex = nCurIndex;
		if (strPageText != L"状态")
			UpdateWindowInfo(m_hCurWnd);
		for(clientNotifyIter iter = m_listClientNotify.begin();
			iter != m_listClientNotify.end(); ++ iter)
		{
			(*iter)->HWNDChanged(m_hCurWnd);
			(*iter)->DragEnd();
		}
		m_hPreWnd = m_hCurWnd;
	}
	return 0;
}
